import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { retry } from 'rxjs/operators';
import { Subscriber } from 'rxjs/Subscriber';
@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: []
})
export class RxjsComponent implements OnInit {

  constructor() { 
    
    this.regresaObservable()
    .subscribe(
    numero => console.log('Subs',numero),
    error => console.log('Error en el obs',error),
    () => console.log('El observador termino')
    );



  }

  ngOnInit() {
  }

    regresaObservable(): Observable<any>{
      let contador = 0;
       return new Observable( (observer: Subscriber<any>) =>{
        let intervalo = setInterval(() => {
          contador += 1;
          observer.next(contador);

          if( contador === 3 ){
            clearInterval(intervalo);
            observer.complete();
          }
          
        },1000);
      });
    }
}
